﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
	public sealed class Singleton
	{
		// from: C# in Depth - Implementing the Singleton Pattern in C#
		// Url: http://csharpindepth.com/Articles/General/Singleton.aspx

		#region Private Fields

		private static readonly Lazy<Singleton> _lazy =
			new Lazy<Singleton>(() => new Singleton());

		#endregion Private Fields

		#region Private Constructors

		private Singleton()
		{
		}

		#endregion Private Constructors

		#region Public Properties

		public static Singleton Instance
		{
			get
			{
				return _lazy.Value;
			}
		}

		#endregion Public Properties
	}
}